summary: Flutter : How to write your own Pop Quiz App using Flutter 
id: popquiz-flutter
categories: Sample
tags: medium
status: Published 
authors: Kenneth Phang
Feedback Link: https://pithermal-cam.asia/

# Flutter : How to write your own Pop Quiz App using Flutter 
<!-- ------------------------ -->
## Overview 
Duration: 1

### What You’ll Learn 
- Stateful Widget
- Stateless Widget
- Network Image 
- Firebase CloudStore Integration 
- Model
- Provider
- Animation
- Material widget
- Text styling
- Navigation (Routes)

![alt-text-here](images/kanban_board.jpg)

(https://api.flutter.dev/index.html)

<!-- ------------------------ -->
## Setting up flutter project
Duration: 2

In order to generate a new flutter project, launch your OS terminal run the following command

```bash
$ flutter create helloworld
$ cd helloworld
```

<!-- ------------------------ -->
## Setup firebase for both Android and iOS intergration
Duration: 15

### Create a cloud store database

![alt-text-here](images/2.jpg)

### Copy the json to android directory

![alt-text-here](images/1.jpg)

### Copy the GoogleService-info.plist to ios directory

![alt-text-here](images/3.jpg)

Google services use CocoaPods to install and manage dependencies. Open a terminal window and navigate to the location of the Xcode project for your app.

Create a Podfile if you don't have one:

```
pod init
```

Open your Podfile and add:

# add the Firebase pod for Google Analytics

```
pod 'Firebase/Analytics'
# add pods for any other desired Firebase products
# https://firebase.google.com/docs/ios/setup#available-pods
```

Save the file and run:
```
pod install
```

This creates an .xcworkspace file for your app. Use this file for all future development on your application.


<!-- ------------------------ -->
## Open your project using Android Studio
Duration: 2

Launch your Android Studio then open the helloworld directory

![alt-text-here](images/android-studio.jpg)

Navigate to the lib directory, open the main.dart file and remove all the content in it.


<!-- ------------------------ -->
## Service Dart
Duration: 15

Create a service folder/package on using your IDE


### Create a package name it as services and dart code

```
import '../model/question.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

class QuizService extends ChangeNotifier{
  final Firestore _db = Firestore.instance;
  final CollectionReference quizesCollection = Firestore.instance.collection('quizes');
  int _questionNumber = 0;
  int counter = 0;

  List<Question> _questionBank = [];

  // room list from snapshot
  List<Question> _questionListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      print(doc.data['question']);
      Question q =  Question(
          doc.data['question'] ?? '',
          doc.data['answer'] ?? '',
          doc.data['imgurl'] ?? '',
      );
      print(q);
      return q;
    }).toList();
  }

  //get rooms stream
  Stream<List<Question>> get questions {
    quizesCollection.getDocuments().then((QuerySnapshot snapshot){
      snapshot.documents.forEach((f) {
        print('${f.data}}');
        Question q =  Question(
          f.data['question'] ?? '',
          f.data['answer'] ?? '',
          f.data['imgurl'] ?? '',
        );
        print(q.imageUrl);
        _questionBank.add(q);
        counter++;
        print(counter);
      });
    });
    return quizesCollection.snapshots().map(_questionListFromSnapshot);
  }

  void nextQuestion(){
    print("next $_questionNumber");
    if(_questionNumber < _questionBank.length-1){
      _questionNumber++;
    }
  }

  String getQuestionText(){
    return _questionBank[_questionNumber].questionText;
  }

  String getQuestionImageUrl(String text){
    print('Text > ' + text);
    var foundObj = _questionBank.singleWhere((p)=>p.questionText == text
        , orElse: () => null);
    print(foundObj.questionText);
    return foundObj.imageUrl;
  }

  bool getCorrectAnswer(){
    return _questionBank[_questionNumber].questionAnswer;
  }

  int getTotalQuestions(){
    return _questionBank.length;
  }

  bool isFinished(){
    print(_questionNumber);
    if(_questionNumber == _questionBank.length-1){
      print("finish");
      return true;
    } else {
      return false;
    }
  }

  void reset(){
    print('reset...');
    _questionNumber=-2;
  }
}
```

<!-- ------------------------ -->
## Main Dart
Duration: 29

### Import your first package

In order to able to instantiate the Material class import the package

```dart
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vector_math/vector_math_64.dart';
import 'package:provider/provider.dart';
import 'screen/home.dart';
import 'services/quiz_service.dart';
```

### Create main.dart for your popquiz app.

```dart
void main() => runApp(MyApp());
```

### Declare all the necessary variables

```
final yellow = Color(0xFFFFFF00);

final textStyle = TextStyle(
  color: yellow,
  fontSize: 24,
);

final textStyleTitle = TextStyle(
  color: yellow,
  fontSize: 28,
);

final crawlString1 = """
Star Wars Pop Quiz
""";
final crawlString2 = """
\nAre you just now getting into Star Wars? Better late than never! 
\nThere's a lot to know about this amazing franchise. 
\nHere's an easy Star Wars trivia quiz for you to try - see how much you know right now!.
\nStar Wars is an American epic space-opera media franchise created by George Lucas. 
\nThe franchise began with the eponymous 1977 film. 
\nQuickly became a worldwide pop-culture phenomenon.
\nNUS ISS - Always learning and Always leading.
""";
```

### Create the app Stateless Widget
- Setting material 
- background image
- Define navigation routes
- Initialize provider throughout the codes
- Enable change notifier when for the firebase record

```
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        builder: (context) {
          QuizService q= QuizService();
          q.questions;
          return q;
        },
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: Scaffold(
            backgroundColor: Color(0xFF000000),
            body:
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage("images/bg.jpg"), fit: BoxFit.cover),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: CrawlPage(),
              ),
            ),
          ),
          routes: <String, WidgetBuilder>{
            '/HomeScreen': (BuildContext context) => new QuizPage()
          },
        ),
      );
  }
}

class CrawlPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Flexible(
        flex: 5,
        child: Perspective(child: Crawler()),
      ),
      Flexible(
        flex: 1,
        child: Column(),
      ),
    ]);
  }
}

class Crawler extends StatefulWidget {
  final crawlDuration = const Duration(seconds: 20);

  @override
  createState() => _CrawlerState();
}

class _CrawlerState extends State<Crawler> {
  final _scrollController = ScrollController();

  _scrollListener() {
    if (_scrollController.offset >= _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        print("reach the bottom");
        Navigator.of(context).pushReplacementNamed('/HomeScreen');
      });
    }
  }
  void gotoNextPage(){
    _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: widget.crawlDuration,
        curve: Curves.linear);
    _scrollController.addListener(_scrollListener);
  }

  @override
  void initState() {
    Future.delayed(
        const Duration(milliseconds: 2000),gotoNextPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    //double width = MediaQuery.of(context).size.width;

    return SuperSize(
      height: 1279,
      child: ListView(
        controller: _scrollController,
        children: [
          SizedBox(height: height),
          Text(
            crawlString1,
            style: textStyleTitle,
            textAlign: TextAlign.center,
          ),
          Text(
            crawlString2,
            style: textStyle,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: height),
        ],
      ),
    );
  }
}
```

Create a stateless widget that imposes different constraints on its child than it gets from its parent, possibly allowing the child to overflow the parent.

```
class SuperSize extends StatelessWidget {
  SuperSize({this.child, this.height = 1000});
  final Widget child;
  final double height;

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      minHeight: height,
      maxHeight: height,
      child: child,
    );
  }
}
```



```
class Perspective extends StatelessWidget {
  Perspective({this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.identity()
        ..setEntry(3, 2, 0.002)
        ..rotateX(-3.14 / 3.5),
      alignment: FractionalOffset.center,
      child: child,
    );
  }
}
```

<!-- ------------------------ -->
## Hyperlinking and Embedded Images
Duration: 1
### Hyperlinking
[Youtube - Halsey Playlists](https://www.youtube.com/user/iamhalsey/playlists)

### Images
![alt-text-here](images/android-studio.jpg)

<!-- ------------------------ -->
## Other Stuff
Duration: 1
