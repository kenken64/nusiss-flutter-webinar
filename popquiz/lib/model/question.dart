/**
 * Model for question
 */
class Question {
  String questionText;
  bool questionAnswer;
  String _imageUrl;

  String get imageUrl => _imageUrl;

  set imageUrl(String imageUrl) {
    _imageUrl = imageUrl;
  }

  Question(this.questionText, this.questionAnswer, this._imageUrl);
}