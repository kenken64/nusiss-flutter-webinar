import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../services/quiz_service.dart';
import 'package:provider/provider.dart';

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Icon> scoreKeeper = [];
  int correctCounter = 0;

  void checkAnswer(bool userPickedAnswer) {
    bool correctAnswer = Provider.of<QuizService>(context).getCorrectAnswer();
    setState(() {
      var congratzMesg = 'Well done!';
      if (Provider.of<QuizService>(context).isFinished() == true &&
          scoreKeeper.length ==  Provider.of<QuizService>(context).getTotalQuestions()) {
        for(final e in scoreKeeper){
          if(e.color == Colors.green){
            correctCounter++;
          }
        }
        Alert(
          context: context,
          type: AlertType.success,
          title: "Congratulations!",
          desc:
              "You have reached the end of the quiz. Your score is ${correctCounter}/${Provider.of<QuizService>(context).getTotalQuestions()}.${congratzMesg}",
          buttons: [
            DialogButton(
              child: Text(
                "Restart Quiz",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                  setState(() {
                    correctCounter = 0;
                    scoreKeeper.clear();
                  });
                  return Navigator.pop(context);
               },
              width: 120,
            )
          ],
        ).show();
        Provider.of<QuizService>(context).reset();

      } else {
        if (userPickedAnswer == correctAnswer) {
          scoreKeeper.add(Icon(Icons.check, color: Colors.green));
        } else {
          scoreKeeper.add(Icon(Icons.close, color: Colors.red));
        }
        if(Provider.of<QuizService>(context).isFinished()){
          checkAnswer(userPickedAnswer);
        }
      }
      Provider.of<QuizService>(context).nextQuestion();
    });
  }

  @override
  Widget build(BuildContext context)  {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(
                Provider.of<QuizService>(context).getQuestionImageUrl(Provider.of<QuizService>(context).getQuestionText())),
            fit: BoxFit.cover),
      ),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Container(
                  child: Text(Provider.of<QuizService>(context).getQuestionText(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.white,
                      )),
                  alignment: FractionalOffset(0.5, 0.2),
                ),
              ),
            ),
            Expanded(
              child: Container(),
            ),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: FlatButton(
                        color: Colors.green,
                        child: Text('True',
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white,
                            )),
                        onPressed: () {
                          // user picked false.
                          checkAnswer(true);
                        }))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: FlatButton(
                        color: Colors.red,
                        child: Text('False',
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.white,
                            )),
                        onPressed: () {
                          // user picked false.
                          checkAnswer(false);
                        }))),
            Row(
              children: scoreKeeper,
            )
          ]),
    );
  }
}
