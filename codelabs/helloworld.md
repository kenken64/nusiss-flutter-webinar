summary: Flutter : Write your first Hello World 
id: helloworld-flutter
categories: Sample
tags: medium
status: Published 
authors: Kenneth Phang
Feedback Link: https://pithermal-cam.asia/

# Flutter : Write your first Hello World 
<!-- ------------------------ -->
## Overview 
Duration: 1

### What You’ll Learn 
- Material Widget
- Stateless Widget 
- Center Widget 
- Text widget
- Appbar widget
- Text styling

<!-- ------------------------ -->
## Setting up flutter
Duration: 2

In order to generate a new flutter project, launch your OS terminal run the following command

```bash
$ flutter create helloworld
$ cd helloworld
```

<!-- ------------------------ -->
## Open your project using Android Studio
Duration: 2

Launch your Android Studio then open the helloworld directory

![alt-text-here](images/android-studio.jpg)

Navigate to the lib directory, open the main.dart file and remove all the content in it.

### Import your first package

In order to able to instantiate the Material class import the package

```dart
import 'package:flutter/material.dart';
```

### Create main entry point for your flutter app

```dart
for (statement 1; statement 2; statement 3) {
  // code block to be executed
}
```

<!-- ------------------------ -->
## Hyperlinking and Embedded Images
Duration: 1
### Hyperlinking
[Youtube - Halsey Playlists](https://www.youtube.com/user/iamhalsey/playlists)

### Images
![alt-text-here](assets/puppy.jpg)

<!-- ------------------------ -->
## Other Stuff
Duration: 1
