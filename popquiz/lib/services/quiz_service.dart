import '../model/question.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

class QuizService extends ChangeNotifier{
  final CollectionReference quizesCollection = Firestore.instance.collection('quizes');
  int _questionNumber = 0;
  int counter = 0;

  List<Question> _questionBank = [];

  // room list from snapshot
  List<Question> _questionListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      Question q =  Question(
          doc.data['question'] ?? '',
          doc.data['answer'] ?? '',
          doc.data['imgurl'] ?? '',
      );
      return q;
    }).toList();
  }

  //get rooms stream
  Stream<List<Question>> get questions {
    quizesCollection.getDocuments().then((QuerySnapshot snapshot){
      snapshot.documents.forEach((f) {
        Question q =  Question(
          f.data['question'] ?? '',
          f.data['answer'] ?? '',
          f.data['imgurl'] ?? '',
        );
        _questionBank.add(q);
        counter++;
      });
    });
    return quizesCollection.snapshots().map(_questionListFromSnapshot);
  }

  void nextQuestion(){
    print("next $_questionNumber");
    if(_questionNumber < _questionBank.length-1){
      _questionNumber++;
    }
  }

  String getQuestionText(){
    return _questionBank[_questionNumber].questionText;
  }

  String getQuestionImageUrl(String text){
    print('Text > ' + text);
    var foundObj = _questionBank.singleWhere((p)=>p.questionText == text
        , orElse: () => null);
    print(foundObj.questionText);
    return foundObj.imageUrl;
  }

  bool getCorrectAnswer(){
    return _questionBank[_questionNumber].questionAnswer;
  }

  int getTotalQuestions(){
    return _questionBank.length;
  }

  bool isFinished(){
    if(_questionNumber == _questionBank.length-1){
      return true;
    } else {
      return false;
    }
  }

  void reset(){
    _questionNumber=-2;
  }
}